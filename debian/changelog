libkf5ksieve (4:18.08.3-2+deb10u1) buster-security; urgency=medium

  * Non-maintainer upload by the LTS Team.
  * CVE-2023-52723: Cleartext password in server logs.

 -- Adrian Bunk <bunk@debian.org>  Sun, 05 May 2024 18:20:57 +0300

libkf5ksieve (4:18.08.3-2) unstable; urgency=medium

  * Team upload.

  [ Sandro Knauß ]
  * Update symbols from buildds for 4:18.08.3

 -- Sandro Knauß <hefee@debian.org>  Thu, 14 Feb 2019 21:08:15 +0100

libkf5ksieve (4:18.08.3-1) unstable; urgency=medium

  * Team upload.

  [ Sandro Knauß ]
  * New upstream release (18.08.3).
  * Update build-deps and deps with the info from cmake.
  * Bump Standards-Version to 4.3.0 (No changes needed).
  * Removed acc autopkgtest.
  * Removed testsuite autopkgtest.

 -- Sandro Knauß <hefee@debian.org>  Mon, 04 Feb 2019 16:02:19 +0100

libkf5ksieve (4:18.08.1-1) unstable; urgency=medium

  * Team upload.

  [ Sandro Knauß ]
  * Bump Standards-Version to 4.2.1 (No changes needed).
  * Update symbols from buildds for 4:18.07.90
  * Follow stable releases in KDE again.
  * New upstream release (18.08.1).
  * Update build-deps and deps with the info from cmake.

 -- Sandro Knauß <hefee@debian.org>  Tue, 02 Oct 2018 23:20:13 +0200

libkf5ksieve (4:18.07.90-1) experimental; urgency=medium

  * Team upload.

  [ Sandro Knauß ]
  * Update watch file to match unstable releases, too.
  * New upstream release (18.07.90).
  * Bump Standards-Version to 4.2.0 (No changes needed).
  * Update build-deps and deps with the info from cmake.
  * Update symbols from build for 18.07.90.
  * Update copyright file for new upstream.

 -- Sandro Knauß <hefee@debian.org>  Sat, 18 Aug 2018 12:14:39 +0200

libkf5ksieve (4:17.12.3-1) unstable; urgency=medium

  * Team upload.

  [ Sandro Knauß ]
  * New upstream release (17.12.3).
  * Update symbols from buildds for 4:17.12.2
  * Update build-deps and deps with the info from cmake.
  * use secure copyright format uri.
  * Update descriptions.

 -- Sandro Knauß <hefee@debian.org>  Sat, 31 Mar 2018 18:27:30 +0200

libkf5ksieve (4:17.12.2-2) experimental; urgency=medium

  * Team upload.

  [ Sandro Knauß ]
  * Update symbols from buildds for 4:17.12.2

 -- Sandro Knauß <hefee@debian.org>  Sun, 25 Mar 2018 16:10:12 +0200

libkf5ksieve (4:17.12.2-1) experimental; urgency=medium

  * Team upload.

  [ Sandro Knauß ]
  * New upstream release (17.12.2).
  * Update Vcs links to salsa.
  * Update build-deps and deps with the info from cmake
  * Update copyright file.
  * Enable build tests (make them non-failing).
  * Add symbols files.
  * Bump debhelper build-dep and compat to 11.

 -- Sandro Knauß <hefee@debian.org>  Fri, 09 Mar 2018 15:15:30 +0100

libkf5ksieve (4:17.08.3-2) unstable; urgency=medium

  * Team upload.

  [ Pino Toscano ]
  * Remove the kdepimlibs-kio-plugins breaks/replaces, since that package is
    for kdepimlibs 4.x.
  * Simplify watch file, and switch it to https.
  * Bump Standards-Version to 4.1.3, no changes required.
  * Remove alternative libqt5webengine5-dev build dependency.
  * Tighten the inter-library dependencies.

  [ Sandro Knauß ]
  * mark kio-sieve as Multi-Arch: same instead of no

 -- Pino Toscano <pino@debian.org>  Wed, 03 Jan 2018 13:07:03 +0100

libkf5ksieve (4:17.08.3-1) unstable; urgency=medium

  * Team upload.

  [ Sandro Knauß ]
  * New upstream release (17.08.3).
  * Get rid of unnessary break/replaces of kde-l10n-(ast|eo).
  * Bump Standards-Version to 4.1.2 (No changes needed).
  * Remove not needed Build-Deps.
  * Bump debhelper build-dep and compat to 10.
  * Set l10npkgs_firstversion_ok to 4:16.04.3-9~

 -- Sandro Knauß <hefee@debian.org>  Thu, 21 Dec 2017 17:59:41 +0100

libkf5ksieve (4:17.08.0-1) experimental; urgency=medium

  * New upstream release (17.08.0)
  * Update copyright file
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Fri, 01 Sep 2017 22:08:41 +0200

libkf5ksieve (4:17.08.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Thu, 17 Aug 2017 09:43:14 +0000

libkf5ksieve (4:17.04.3-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 12 Jul 2017 10:25:38 +0000

libkf5ksieve (4:17.04.2-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 07 Jun 2017 12:24:34 +0000

libkf5ksieve (4:17.04.1-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Thu, 11 May 2017 18:26:31 +0000

libkf5ksieve (4:17.04.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Thu, 20 Apr 2017 09:32:43 +0000

libkf5ksieve (4:16.12.3-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 08 Mar 2017 13:51:22 +0000

libkf5ksieve (4:16.12.2-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 08 Feb 2017 17:04:13 +0000

libkf5ksieve (4:16.12.1-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 11 Jan 2017 13:24:56 +0000

libkf5ksieve (4:16.12.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 14 Dec 2016 16:33:03 +0000

libkf5ksieve (4:16.08.3-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 21 Nov 2016 13:54:54 +0000

libkf5ksieve (4:16.08.2-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 14 Oct 2016 13:41:33 +0000

libkf5ksieve (4:16.08.1-1~1.gbp056676) UNRELEASED; urgency=medium

  ** SNAPSHOT build @0566769707cf922e100ddffc50017322e6c7e214 **

  [ Maximiliano Curia ]
  ** SNAPSHOT build @ead7624982d1520b79a31c182580e695fb7025d6 **

  * UNRELEASED

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake
  * Refresh patches
  * Update build-deps and deps with the info from cmake

 -- Automatic packaging <maxy+jenkins@gnuservers.com.ar>  Thu, 08 Sep 2016 23:04:36 +0200

libkf5ksieve (4:16.08.1-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 16 Sep 2016 13:08:19 +0000

libkf5ksieve (4:16.08.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 17 Aug 2016 11:50:35 +0000

libkf5ksieve (4:16.04.3-2) unstable; urgency=medium

  * Team upload

  [ Didier Raboud ]
  * Backport 5 upstream patches to fix connection unreliability after
    some time, thanks to Maximilian Engelhardt for the report and
    pointing to the commits (Closes: #850462)

 -- Didier Raboud <odyx@debian.org>  Wed, 25 Jan 2017 18:36:05 +0100

libkf5ksieve (4:16.04.3-1) unstable; urgency=medium

  * New upstream release (16.04.3)
    (Closes: 841458) Thanks to Maximilian Engelhardt for the report

 -- Maximiliano Curia <maxy@debian.org>  Fri, 21 Oct 2016 11:01:32 +0200

libkf5ksieve (4:16.04.3-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 03 Aug 2016 12:51:48 +0000

libkf5ksieve (4:16.04.2-2) unstable; urgency=low

  * Release to unstable.

 -- Maximiliano Curia <maxy@debian.org>  Wed, 06 Jul 2016 08:58:44 +0200

libkf5ksieve (4:16.04.2-1) experimental; urgency=medium

  [ Automatic packaging ]
  * Refresh patches

 -- Maximiliano Curia <maxy@debian.org>  Thu, 30 Jun 2016 14:17:43 +0200

libkf5ksieve (4:16.04.1-1) experimental; urgency=medium

  * Initial release.
  * Add missing breaks/replaces
  * Add upstream patch: upstream_add_copying_files.patch

 -- Maximiliano Curia <maxy@debian.org>  Sat, 04 Jun 2016 22:31:51 +0200
